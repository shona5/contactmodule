//
//  mapViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 29/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "mapViewController.h"
#import <MapKit/MKMapView.h>

@interface mapViewController ()
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapview;

@end

@implementation mapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
