
//
//  listTableViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 30/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "listTableViewController.h"
#import "contactDetailViewController.h"

@interface listTableViewController ()
- (IBAction)back:(id)sender;

@end

@implementation listTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
    
    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return result.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    newContact =(Contact*)[result objectAtIndex:indexPath.row];
    NSString *record=(@"%@ %@",newContact.fn,newContact.ln);
    cell.textLabel.text=record;
    
    return cell;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * indexpath = [self.tableView indexPathForSelectedRow];
    contactDetailViewController * vc = segue.destinationViewController;
    
    Contact  * c =[result objectAtIndex:indexpath.row];
    vc.fnstr=c.fn;
//    vc.lnStr=c.ln;
//    vc.emailStr=c.email;
//    vc.websiteStr=c.web;
//    //  vc.faxStr= c.fax;
//    //  vc.phoneStr=c.phoneNum;
//    vc.streetStr=c.street;
//    vc.cityStr=c.city;
//    vc.stateStr=c.state;
//    vc.countryStr=c.country;
//    // vc.zipStr=c.zip;
//    vc.fbStr=c.fb;
//    vc.tweetStr=c.tweet;
//    vc.linkStr=c.link;
//    
    
    
    
}



- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
