//
//  cardsViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 29/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "cardsViewController.h"

@interface cardsViewController ()
- (IBAction)back:(id)sender;

@end

@implementation cardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
