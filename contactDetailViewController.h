//
//  contactDetailViewController.h
//  tableContactCRM
//
//  Created by Don Asok on 30/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contactDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,retain)NSString *fnstr;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *L1;
@property (weak, nonatomic) IBOutlet UITextField *L2;
@property (weak, nonatomic) IBOutlet UITextField *L3;
@property (weak, nonatomic) IBOutlet UITextField *L4;
@property (weak, nonatomic) IBOutlet UITextField *L5;
@property (weak, nonatomic) IBOutlet UITextField *L6;
@property (weak, nonatomic) IBOutlet UITextField *L7;

@property(nonatomic,retain)NSMutableArray *array;

@end
