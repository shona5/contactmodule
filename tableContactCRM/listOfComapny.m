//
//  listOfComapny.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "listOfComapny.h"

@interface listOfComapny ()

@end

@implementation listOfComapny

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    
    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return result.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    newContact =(Company*)[result objectAtIndex:indexPath.row];
    //NSString *record=(@"%@%@",newContact.fn,newContact.ln);
    cell.textLabel.text=newContact.comp;
    //cell.textLabel.text = newContact.fn;
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * indexpath = [self.tableView indexPathForSelectedRow];
    companyRacordDisplay * vc = segue.destinationViewController;
    
    Company  * c =[result objectAtIndex:indexpath.row];
    vc.compStr=c.comp;
   // vc.mobileStr=c.mobile;
    vc.emailStr=c.email;
    vc.websiteStr=c.web;
    vc.faxStr= [NSString stringWithFormat:@"%@",c.fax];
    vc.phoneStr=[NSString stringWithFormat:@"%@",c.phoneNum];
    vc.streetStr=c.street;
    vc.cityStr=c.city;
    vc.stateStr=c.state;
    vc.countryStr=c.country;
    vc.zipStr=[NSString stringWithFormat:@"%@",c.zip];
    vc.fbStr=c.fb;
    vc.tweetStr=c.tweet;
    vc.linkStr=c.link;
}



@end
