//
//  Company+CoreDataProperties.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Company+CoreDataProperties.h"

@implementation Company (CoreDataProperties)

@dynamic city;
@dynamic country;
@dynamic email;
@dynamic fax;
@dynamic fb;
@dynamic comp;
@dynamic link;
@dynamic mobile;
@dynamic phoneNum;
@dynamic state;
@dynamic street;
@dynamic tweet;
@dynamic web;
@dynamic zip;

@end
