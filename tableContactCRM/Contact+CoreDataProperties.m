//
//  Contact+CoreDataProperties.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact+CoreDataProperties.h"

@implementation Contact (CoreDataProperties)

@dynamic fn;
@dynamic ln;
@dynamic phoneNum;
@dynamic email;
@dynamic web;
@dynamic fax;
@dynamic street;
@dynamic country;
@dynamic zip;
@dynamic state;
@dynamic city;
@dynamic fb;
@dynamic tweet;
@dynamic link;

@end
