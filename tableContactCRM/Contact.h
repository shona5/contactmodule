//
//  Contact.h
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Contact : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Contact+CoreDataProperties.h"
