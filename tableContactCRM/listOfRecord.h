//
//  listOfRecord.h
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
#import "AppDelegate.h"
#import "TableViewController.h"
#import "DisplayViewController.h"


@interface listOfRecord : UITableViewController<UISearchBarDelegate>
{
    NSArray * result;
    Contact * newContact;
    NSMutableArray *filterd;
    BOOL isFiltering;
    NSMutableArray *strarray;

}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
