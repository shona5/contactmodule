//
//  listOfRecord.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "listOfRecord.h"

@interface listOfRecord ()
- (IBAction)back:(id)sender;

@end

@implementation listOfRecord

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
    
    result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    _searchBar.delegate=self;
    [strarray addObjectsFromArray:result];

   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    //return result.count;
    if (isFiltering)
    {
        return filterd.count;
    }
    return result.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    newContact =(Contact*)[result objectAtIndex:indexPath.row];
    NSString *record=(@"%@ %@",newContact.fn,newContact.ln);
    cell.textLabel.text=record;
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
       NSIndexPath * indexpath = [self.tableView indexPathForSelectedRow];
    DisplayViewController * vc = segue.destinationViewController;
    
    Contact  * c =[result objectAtIndex:indexpath.row];
    vc.fnStr=c.fn;
    vc.lnStr=c.ln;
    vc.emailStr=c.email;
    vc.websiteStr=c.web;
  //  vc.faxStr= c.fax;
  //  vc.phoneStr=c.phoneNum;
    vc.streetStr=c.street;
    vc.cityStr=c.city;
    vc.stateStr=c.state;
    vc.countryStr=c.country;
   // vc.zipStr=c.zip;
    vc.fbStr=c.fb;
    vc.tweetStr=c.tweet;
    vc.linkStr=c.link;
    
    
   
    
}


- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length==0)
    {
        isFiltering=NO;
    }
    else
    {
        isFiltering=YES;
        filterd=[[NSMutableArray alloc]init];
//        for (NSString *str in re)
//        {
//            NSRange strRange=[str rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            if (strRange.location!= NSNotFound)
//            {
//                [filterd addObject:str];
//            }
//           
//        }
         NSLog(@"%@",filterd);
        
        for (NSDictionary *item in strarray) {
            NSString *string = [item objectForKey:@"ln"];
            NSRange range = [string rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (range.location != NSNotFound) {
               
                [filterd addObject:item];
            } 
        }
        
        
    }
    [self.tableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.tableView resignFirstResponder];
}



@end
