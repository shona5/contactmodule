//
//  companyRacordDisplay.h
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface companyRacordDisplay : UITableViewController

@property(nonatomic,retain)NSString *compStr,*mobileStr,*emailStr,*websiteStr,*phoneStr,*faxStr,*streetStr,*countryStr,*zipStr,*cityStr,*stateStr,*fbStr,*tweetStr,*linkStr;
@property (weak, nonatomic) IBOutlet UITextField *comp;
@property (weak, nonatomic) IBOutlet UITextField *mobile;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *emailC;
@property (weak, nonatomic) IBOutlet UITextField *websiteC;
@property (weak, nonatomic) IBOutlet UITextField *faxC;
@property (weak, nonatomic) IBOutlet UITextField *streetC;
@property (weak, nonatomic) IBOutlet UITextField *cityC;
@property (weak, nonatomic) IBOutlet UITextField *stateC;
@property (weak, nonatomic) IBOutlet UITextField *zipC;
@property (weak, nonatomic) IBOutlet UITextField *countryC;
@property (weak, nonatomic) IBOutlet UITextField *facebookC;
@property (weak, nonatomic) IBOutlet UITextField *twitterC;
@property (weak, nonatomic) IBOutlet UITextField *linkedInC;

@end
