//
//  TableViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "TableViewController.h"
#import "companyAdd.h"

@interface TableViewController ()
- (IBAction)save:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *website;
@property (weak, nonatomic) IBOutlet UITextField *fax;
@property (weak, nonatomic) IBOutlet UITextField *streetT;
@property (weak, nonatomic) IBOutlet UITextField *cityT;
@property (weak, nonatomic) IBOutlet UITextField *stateT;
@property (weak, nonatomic) IBOutlet UITextField *zipT;
@property (weak, nonatomic) IBOutlet UITextField *coutryT;
@property (weak, nonatomic) IBOutlet UITextField *facebook;
@property (weak, nonatomic) IBOutlet UITextField *twitter;
@property (weak, nonatomic) IBOutlet UITextField *linkedIn;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayComp =[[NSMutableArray alloc]init];
    companyAdd * comp=[[companyAdd alloc]init];
   // comp.array=arrayComp;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (IBAction)save:(id)sender
{
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"fn == %@ AND ln==%@",self.firstName.text,self.lastName.text];
    
  
    
    [request setPredicate:predicate];
    
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    
    if (result.count != 0)
    {
        NSLog(@"Company already exist");
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"contact already exist" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            self.firstName.text = @"";
            self.lastName.text = @"";
            
            [self.firstName becomeFirstResponder];
            
        }];
        
        [alertcontroller addAction:action];
        
        
    }
    else
    {
        Contact * c = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:myDelegate.managedObjectContext];
        
        c.fn = self.firstName.text;
        c.ln = self.lastName.text;
        c.email=self.email.text;
        c.web=self.website.text;
        //c.phoneNum=self.phone.text;
       // c.fax=self.fax.text;
        c.street=self.stateT.text;
        c.country=self.coutryT.text;
        c.street=self.streetT.text;
       // c.zip=self.zipT.text;
        c.city=self.cityT.text;
        c.fb=self.facebook.text;
        c.tweet=self.twitter.text;
        c.link=self.linkedIn.text;
        
        
        
        NSError * error;
        
        [myDelegate.managedObjectContext save:&error];
        
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
            NSLog(@"Contact added successfuly");
        
        alertcontroller = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Contact added successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [alertcontroller addAction:action];
        
        
        
    }

}
@end
