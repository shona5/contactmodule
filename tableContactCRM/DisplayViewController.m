//
//  DisplayViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "DisplayViewController.h"
#import "AppDelegate.h"
#import "Contact.h"

@interface DisplayViewController ()
- (IBAction)Edit:(id)sender;

@end

@implementation DisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fn.text=self.fnStr;
    self.ln.text=self.lnStr;
    self.phone.text=self.phoneStr;
    self.email.text=self.emailStr;
    self.website.text=self.websiteStr;
    self.fax.text=self.faxStr;
    self.zip.text=self.zipStr;
    self.street.text=self.streetStr;
    self.state.text=self.stateStr;
    self.country.text=self.countryStr;
    self.city.text=self.cityStr;
    self.facebook.text=self.fbStr;
    self.twitter.text=self.tweetStr;
    self.linkedIn.text=self.linkStr;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)Edit:(id)sender
{
    UIApplication * myapp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = [myapp delegate];
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Contact"];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"fn = %@",self.fn.text ];
    [request setPredicate:predicate];
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    if (result.count == 0)
    {
        NSLog(@"Person Id not found");
        UIAlertController * alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Person Id does not exist " preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.fn.text = @"";
            [self.fn becomeFirstResponder];
        }];
        [alertcontroller addAction:action];
        [self presentViewController:alertcontroller animated:YES completion:nil];
        
    }
    else
    {
        
        Contact * c = [result firstObject ];
        
        
        c.fn = self.fn.text;
        c.ln=self.ln.text;
        c.city=self.city.text;
       // c.phoneNum=self.phone.text;
        c.state=self.state.text;
        c.country=self.country.text;
        c.street=self.street.text;
        //c.zip=self.zip.text;
        c.fb=self.facebook.text;
        c.tweet=self.twitter.text;
        c.link=self.linkedIn.text;
        c.email=self.email.text;
        c.web=self.website.text;
        
        NSError * error;
        
        [myDelegate.managedObjectContext save:&error];
        
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
        {
            NSLog(@"Person name updated successfuly");
            
            UIAlertController  * alertcontroller1 = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company name  updated successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
            ;
            
            
            UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertcontroller1 addAction:action1];
            
            [self presentViewController:alertcontroller1 animated:YES completion:nil];
        }
        
        
    }
    

}
@end
