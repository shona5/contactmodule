//
//  companyRacordDisplay.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "companyRacordDisplay.h"
#import "AppDelegate.h"
#import "Company.h"

@interface companyRacordDisplay ()

@end

@implementation companyRacordDisplay

- (void)viewDidLoad {
    [super viewDidLoad];
    self.comp.text=self.compStr;
    self.mobile.text=self.mobileStr;
    self.emailC.text=self.emailStr;
    self.websiteC.text=self.websiteStr;
    self.streetC.text=self.streetStr;
    self.countryC.text=self.countryStr;
    self.stateC.text=self.stateStr;
    self.zipC.text=self.zipStr;
    self.cityC.text=self.cityStr;
    self.faxC.text=self.faxStr;
    self.facebookC.text=self.fbStr;
    self.twitterC.text=self.tweetStr;
    self.linkedInC.text=self.linkStr;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
- (IBAction)editCompany:(id)sender
{
            UIApplication * myapp = [UIApplication sharedApplication];
        AppDelegate * myDelegate = [myapp delegate];
        NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"comp = %@",self.comp.text ];
        [request setPredicate:predicate];
        NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
        
        if (result.count == 0)
        {
            NSLog(@"Company name not found");
            UIAlertController * alertcontroller = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Comapny does not exist " preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.comp.text = @"";
                [self.comp becomeFirstResponder];
            }];
            [alertcontroller addAction:action];
            [self presentViewController:alertcontroller animated:YES completion:nil];
            
        }
        else
        {
            
            Company * c = [result firstObject ];
            
            
            c.comp = self.comp.text;
           // c.mobile=self.mobile.text;
            c.city=self.cityC.text;
           // c.phoneNum=self.phone.text;
            c.state=self.stateC.text;
            c.country=self.countryC.text;
            c.street=self.streetC.text;
         //   c.zip=self.zipC.text;
            c.fb=self.facebookC.text;
            c.tweet=self.twitterC.text;
            c.link=self.linkedInC.text;
            c.email=self.emailC.text;
            c.web=self.websiteC.text;
            
            NSError * error;
            
            [myDelegate.managedObjectContext save:&error];
            
            if (error)
            {
                NSLog(@"Error = %@",error.localizedDescription);
            }
            else
            {
                NSLog(@"Person name updated successfuly");
                
                UIAlertController  * alertcontroller1 = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company name  updated successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
                ;
                
                
                UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                
                [alertcontroller1 addAction:action1];
                
                [self presentViewController:alertcontroller1 animated:YES completion:nil];
            }
            
            
        }
        
        
    }




@end
