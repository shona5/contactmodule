//
//  companyAdd.m
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "companyAdd.h"

@interface companyAdd ()
@property (weak, nonatomic) IBOutlet UITextField *compName;
@property (weak, nonatomic) IBOutlet UITextField *mobile;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *website;
@property (weak, nonatomic) IBOutlet UITextField *fax;
@property (weak, nonatomic) IBOutlet UITextField *street;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UITextField *zip;
@property (weak, nonatomic) IBOutlet UITextField *country;
@property (weak, nonatomic) IBOutlet UITextField *facebook;
@property (weak, nonatomic) IBOutlet UITextField *twitter;
@property (weak, nonatomic) IBOutlet UITextField *linkedIn;
- (IBAction)save:(id)sender;

@end

@implementation companyAdd

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *temp=self.compName.text;
    array=[[NSMutableArray alloc]init];
    [array addObject:temp];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender
{
    UIApplication * myApp = [UIApplication sharedApplication];
    AppDelegate * myDelegate = myApp.delegate;
    
    
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"comp == %@",self.compName.text];
    
    
    
    [request setPredicate:predicate];
    
    NSArray * result = [myDelegate.managedObjectContext executeFetchRequest:request error:nil];
    
    
    if (result.count != 0)
    {
        NSLog(@"Company already exist");
        alertController = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Company already exist" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            self.compName.text = @"";
            
            
            [self.compName becomeFirstResponder];
            
        }];
        
        [alertController addAction:action];
        
        
    }
    else
    {
        Company * c = [NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:myDelegate.managedObjectContext];
        
        c.comp = self.compName.text;
       // c.mobile = self.mobile.text;
        c.email=self.email.text;
        c.web=self.website.text;
        // c.phoneNum=[NSString stringWithFormat:@"%d",[self.phone.text intValue]];
        //c.fax=[NSString stringWithFormat:@"%d",[self.fax.text intValue]];
        c.street=self.state.text;
        c.country=self.country.text;
        c.street=self.street.text;
        // c.zip=[NSString stringWithFormat:@"%ld",[self.zipT.text integerValue]];
        c.city=self.city.text;
        c.fb=self.facebook.text;
        c.tweet=self.twitter.text;
        c.link=self.linkedIn.text;
        
        
        
        NSError * error;
        
        [myDelegate.managedObjectContext save:&error];
        
        if (error)
        {
            NSLog(@"Error = %@",error.localizedDescription);
        }
        else
            NSLog(@"Contact added successfuly");
        
        alertController = [UIAlertController alertControllerWithTitle:@"Welcome" message:@"Company added successfuly" preferredStyle:(UIAlertControllerStyleActionSheet)]
        ;
        [self presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [alertController addAction:action];
        
        
        
    }
    

}
@end
