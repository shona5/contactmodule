//
//  generalTableViewController.m
//  tableContactCRM
//
//  Created by Don Asok on 29/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "generalTableViewController.h"

@interface generalTableViewController ()
- (IBAction)back:(id)sender;

@end

@implementation generalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   }

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
