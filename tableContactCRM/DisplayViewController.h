//
//  DisplayViewController.h
//  tableContactCRM
//
//  Created by Don Asok on 23/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *fn;
@property (weak, nonatomic) IBOutlet UITextField *ln;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property(nonatomic,retain)NSString *fnStr,*lnStr,*emailStr,*websiteStr,*phoneStr,*faxStr,*streetStr,*countryStr,*zipStr,*cityStr,*stateStr,*fbStr,*tweetStr,*linkStr;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *website;
@property (weak, nonatomic) IBOutlet UITextField *fax;
@property (weak, nonatomic) IBOutlet UITextField *street;
@property (weak, nonatomic) IBOutlet UITextField *state;
@property (weak, nonatomic) IBOutlet UITextField *zip;
@property (weak, nonatomic) IBOutlet UITextField *country;
@property (weak, nonatomic) IBOutlet UITextField *facebook;
@property (weak, nonatomic) IBOutlet UITextField *twitter;
@property (weak, nonatomic) IBOutlet UITextField *linkedIn;

@property (weak, nonatomic) IBOutlet UITextField *city;
@end
